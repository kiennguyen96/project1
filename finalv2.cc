#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/buildings-helper.h"
#include "ns3/applications-module.h"
#include "ns3/epc-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/internet-module.h"
#include "ns3/config-store.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/animation-interface.h"
#include "ns3/network-module.h"
#include "ns3/ipv4.h"
/*
 * create an lteHelper-only simulation without EPC
 */

using namespace ns3;
NS_LOG_COMPONENT_DEFINE("LteFirstExample");
int main(int argc,char*argv[]){
	// uint16_t numberOfNodes = 6;
	uint16_t numberOfUEs = 10;
	uint16_t numberOfeNBs= 1;
	double simTime = 1;
	double enbdistx = 0;
	double enbdisty = 0;
	double interPacketInterval = 100;
  	int PacketSize = 3000;
	// char center[6] ="0.0"; 
	uint16_t AppMaxPacket =3000;
	uint16_t AppMaxPacket1 =3000;
	CommandLine cmd;
	cmd.AddValue("simTime", "Total duration of the simulation [s])", simTime);
	// cmd.AddValue("distance", "Distance between eNBs in x and y [m]", enbdistx, enbdisty);
	// cmd.AddValue("EnableFlowMonitor", "Enable Flow flowMonitor", enableFlowMonitor);
	cmd.AddValue("interPacketInterval", "Inter packet interval [ms])", interPacketInterval);
	cmd.Parse(argc, argv);
	Config::SetDefault ("ns3::LteAmc::AmcModel", EnumValue (LteAmc::PiroEW2010));
	Config::SetDefault ("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue(40));
	Config::SetDefault ("ns3::LteUePhy::TxPower", DoubleValue(23));
	Config::SetDefault ("ns3::LteUePhy::NoiseFigure", DoubleValue(5));
	Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue(false));
	Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue(false));
  Config::SetDefault ("ns3::LteEnbNetDevice::DlBandwidth", UintegerValue(25));
	ConfigStore inputConfig;
	inputConfig.ConfigureDefaults();
	cmd.Parse(argc, argv);

	NS_LOG_INFO("Create lteHelper NET");
	LogComponentEnable("LteFirstExample",LOG_LEVEL_INFO);
//create a ltehelper object  epc object
	Ptr<LteHelper> lteHelper=CreateObject<LteHelper>();
	Ptr<PointToPointEpcHelper>	epcp2pHelper = CreateObject<PointToPointEpcHelper> ();
	// Ptr <EpcHelper> epcHelper = CreateObject<EpcHelper>();
	lteHelper->SetEpcHelper (epcp2pHelper);
	// lteHelper->SetFadingModel("ns3::TraceFadingLossModel");
	// lteHelper->SetFadingModelAttribute("TraceFilename",StringValue("src/lteHelper/model/fading-traces/fading_trace_EPA_3kmph.fad"));
	// lteHelper->SetFadingModelAttribute("TraceLength",TimeValue(Seconds(10.0)));
	// lteHelper->SetFadingModelAttribute("SamplesNum",UintegerValue(10000));
	// lteHelper->SetFadingModelAttribute("WindowSize",TimeValue(Seconds(0.5)));
	// lteHelper->SetFadingModelAttribute("RbNum",UintegerValue(100));


	NodeContainer enbNodes;
	enbNodes.Create(numberOfeNBs);
	NodeContainer ueNodes;
	ueNodes.Create(numberOfUEs);

	MobilityHelper mobility;
	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
		// for (uint16_t i = 0; i < numberOfeNBs; i++)
		// 	{
		// 		positionAlloc->Add (Vector(enbdistx * i,enbdisty, 0));
		// 	}
	positionAlloc->Add (Vector (enbdistx, enbdisty, 0));
	mobility.SetPositionAllocator(positionAlloc);
	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	// mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator", 
	// "X", StringValue ("50.0"), 
	// "Y", StringValue ("50.0"), 
	// "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=30]")); 

	mobility.Install(enbNodes);//(0,0,0)

	// mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator", 
	// "X", StringValue (center), 
	// "Y", StringValue (center), 
	// "Rho", StringValue ("ns3::UniformRandomVariable[Min=1000|Max=5000]")); 
	// mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
	// 	"MinX", DoubleValue (-15.0),
	// 	"MinY", DoubleValue (-15.0),
	// 	"DeltaX", DoubleValue (10.0),
	// 	"DeltaY", DoubleValue (10.0),
	// 	"GridWidth", UintegerValue (4),
	// 	"LayoutType", StringValue ("RowFirst"));
	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
		// mobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel",
		// "Mode", StringValue ("Time"), 
		// "Time", StringValue ("2s"), 
		// "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"), 
		// "Bounds", StringValue ("0|1000|0|1000")); 
	mobility.Install(ueNodes); 
		// BuildingsHelper::Install (ueNodes); 
		// Default scheduler is PF, uncomment to use RR 
		// lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");
		// lteHelper->SetSchedulerType ("ns3::FdBetFfMacScheduler");	 // FD-BET scheduler
		// lteHelper->SetSchedulerType ("ns3::TdBetFfMacScheduler");	 // TD-BET scheduler
		lteHelper->SetSchedulerType ("ns3::FdMtFfMacScheduler");		// FD-MT scheduler
		// lteHelper->SetSchedulerType ("ns3::TdMtFfMacScheduler");		// TD-MT scheduler
		// lteHelper->SetSchedulerType ("ns3::PfFfMacScheduler");		//FD-PF scheduler
		// lteHelper->SetSchedulerType ("ns3::FdTbfqFfMacScheduler");	// FD-TBFQ scheduler
		// lteHelper->SetSchedulerType ("ns3::TdTbfqFfMacScheduler");	// TD-TBFQ scheduler
		// lteHelper->SetSchedulerType ("ns3::PssFfMacScheduler");		 //PSS scheduler
		// lteHelper->SetSchedulerAttribute ("CqiTimerThreshold", UintegerValue (3));
		// lteHelper->SetSchedulerType ("ns3::CqaFfMacScheduler");		//FD-TD CQA Scheduler
		// Config::SetDefault ("ns3::CqaFfMacScheduler::CqaMetric", StringValue("CqaPf"));
	 // Install LTE Devices to the nodes
		NetDeviceContainer enbDevs;
		enbDevs=lteHelper->InstallEnbDevice(enbNodes);
		NetDeviceContainer ueDevs;
		ueDevs=lteHelper->InstallUeDevice(ueNodes);
		// Ptr<Node> ctrenb=enbDevs.Get(0);


	//Attach the UEs to an eNB with more than 2 eNBs.
	// 	for (uint16_t i = 0; i < numberOfUEs; i++){
	// 		if ( i % numberOfeNBs != 0 ){
	// 			lteHelper->Attach (ueDevs.Get(i), enbDevs.Get(0));
	// 	}	
	// 		else{
	// 			if ( i % numberOfeNBs != 1 )
	// 					lteHelper->Attach (ueDevs.Get(i), enbDevs.Get(1));
	// 			else{
	// 					lteHelper->Attach (ueDevs.Get(i), enbDevs.Get(2));
	// 		}
	// 	}
	// }

		Ptr<Node> pgw = epcp2pHelper->GetPgwNode ();

		 // Create a single RemoteHost
		NodeContainer remoteHostContainer;
		remoteHostContainer.Create (1);
		Ptr<Node> remoteHost = remoteHostContainer.Get (0);
		InternetStackHelper internet;
		internet.Install (remoteHost);

		// Create the Internet
		PointToPointHelper p2ph;
		p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
		p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
		p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
		NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
		Ipv4AddressHelper ipv4h;
		// ipv4h.SetBase ("10.0.0.0", "255.255.255.0", "0.0.0.1");
		ipv4h.SetBase ("1.0.0.0", "255.255.255.0");

		Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
		// interface 0 is localhost, 1 is the p2p device
		Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

		Ptr<ListPositionAllocator> positionAlloc1 = CreateObject<ListPositionAllocator> ();

		positionAlloc1->Add (Vector (enbdistx-400, enbdisty, 0));
		mobility.SetPositionAllocator(positionAlloc1);
		mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

		mobility.Install (remoteHostContainer);


		Ptr<ListPositionAllocator> positionAlloc2 = CreateObject<ListPositionAllocator> ();

		positionAlloc2->Add (Vector (enbdistx-200, enbdisty+200, 0));
		mobility.SetPositionAllocator(positionAlloc2);
		mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

		mobility.Install (pgw);

		Ipv4StaticRoutingHelper ipv4RoutingHelper;
		Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
		remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1); 
	// //Install and start applications on UEs and remote host
		// NS_LOG_INFO("Assign Ue Ipv4Address");
		 // Install the IP stack on the UEs
		// NS_LOG_INFO("Install internet to UE");
		internet.Install (ueNodes);
		Ipv4InterfaceContainer ueIpIface;
		// Ipv4Address ueAddr = ueIpIface.GetAddress();
		// ipv4h.Ipv4AddressHelper ("192.168.1.0", "255.255.123.0", "0.0.0.1");
		ueIpIface = epcp2pHelper->AssignUeIpv4Address (NetDeviceContainer (ueDevs));
		// ueIpIface = ipv4h.Assign (ueDevs);

	// TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");

		// // Assign IP address to UEs, and install applications
		for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
			{
				Ptr<Node> ueNode = ueNodes.Get (u);
				// // Set the default gateway for the UE

				Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
				ueStaticRouting->SetDefaultRoute (epcp2pHelper->GetUeDefaultGatewayAddress (), 1);
				// lteHelper->ActivateDedicatedEpsBearer (ueDevs.Get(u), EpsBearer (EpsBearer::GBR_CONV_VOICE), EpcTft::Default ());
			}
	// epcHelper->AddEnb(enbNodes,enbDevs,1);
		lteHelper->Attach(ueDevs,enbDevs.Get(0));

		uint16_t dlPort = 1234;
		uint16_t ulPort = 2000;
		uint16_t otherPort = 3000;
		ApplicationContainer clientApps;
		ApplicationContainer serverApps;
		ApplicationContainer clientApps1;
		ApplicationContainer serverApps1;
		for (uint32_t i = 0; i < ueNodes.GetN(); i++)
			{
				if(i < numberOfUEs/2)
				{
					++ulPort;
					++otherPort;
					PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), dlPort));
					PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), ulPort));
					PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), otherPort));
					serverApps.Add (dlPacketSinkHelper.Install (ueNodes.Get(i)));
					serverApps.Add (ulPacketSinkHelper.Install (remoteHost));
					serverApps.Add (packetSinkHelper.Install (ueNodes.Get(i)));

					UdpClientHelper dlClient (ueIpIface.GetAddress (i), dlPort);
					dlClient.SetAttribute ("Interval", TimeValue (MicroSeconds(interPacketInterval)));
					dlClient.SetAttribute ("MaxPackets", UintegerValue(AppMaxPacket));
	        		dlClient.SetAttribute ("PacketSize", UintegerValue(PacketSize));

					UdpClientHelper ulClient (remoteHostAddr, ulPort);
					ulClient.SetAttribute ("Interval", TimeValue (MicroSeconds(interPacketInterval)));
					ulClient.SetAttribute ("MaxPackets", UintegerValue(AppMaxPacket));

					UdpClientHelper client (ueIpIface.GetAddress (i), otherPort);
					client.SetAttribute ("Interval", TimeValue (MicroSeconds(interPacketInterval)));
					client.SetAttribute ("MaxPackets", UintegerValue(AppMaxPacket));

					clientApps.Add (dlClient.Install (remoteHost));
					clientApps.Add (ulClient.Install (ueNodes.Get(i)));
					if (i+1 < numberOfUEs/2)
						{
							clientApps.Add (client.Install (ueNodes.Get(i+1)));
						}
					else
						{
							clientApps.Add (client.Install (ueNodes.Get(0)));
						}
				}
				else
				{
					++ulPort;
					++otherPort;
					PacketSinkHelper dlPacketSinkHelper1 ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), dlPort));
					PacketSinkHelper ulPacketSinkHelper1 ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), ulPort));
					PacketSinkHelper packetSinkHelper1 ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), otherPort));
					serverApps1.Add (dlPacketSinkHelper1.Install (ueNodes.Get(i)));
					serverApps1.Add (ulPacketSinkHelper1.Install (remoteHost));
					serverApps1.Add (packetSinkHelper1.Install (ueNodes.Get(i)));

					UdpClientHelper dlClient1 (ueIpIface.GetAddress (i), dlPort);
					dlClient1.SetAttribute ("Interval", TimeValue (MicroSeconds (interPacketInterval)));
					dlClient1.SetAttribute ("MaxPackets", UintegerValue(AppMaxPacket1));

					UdpClientHelper ulClient1 (remoteHostAddr, ulPort);
					ulClient1.SetAttribute ("Interval", TimeValue (MicroSeconds (interPacketInterval)));
					ulClient1.SetAttribute ("MaxPackets", UintegerValue(AppMaxPacket1));

					UdpClientHelper client1 (ueIpIface.GetAddress (i), otherPort);
					client1.SetAttribute ("Interval", TimeValue (MicroSeconds (interPacketInterval)));
					client1.SetAttribute ("MaxPackets", UintegerValue(AppMaxPacket1));

					clientApps1.Add (dlClient1.Install (remoteHost));
					clientApps1.Add (ulClient1.Install (ueNodes.Get(i)));
					if (i+1 < ueNodes.GetN())
						{
							clientApps1.Add (client1.Install (ueNodes.Get(i+1)));
						}
					else
						{
							clientApps1.Add (client1.Install (ueNodes.Get(0)));
						}
				}
				
			}
		serverApps.Start (Seconds (0.01));
		clientApps.Start (Seconds (0.01));

		serverApps1.Start (Seconds (0.01));
		clientApps1.Start (Seconds (0.01));

	enum EpsBearer::Qci q=EpsBearer::GBR_CONV_VIDEO;
	enum EpsBearer::Qci q1=EpsBearer::NGBR_VOICE_VIDEO_GAMING;
	// enum EpsBearer::Qci q=EpsBearer::GBR_CONV_VIDEO;	
	// enum EpsBearer::Qci q=EpsBearer::GBR_GAMING;
	// enum EpsBearer::Qci q=EpsBearer::NGBR_VOICE_VIDEO_GAMING;

	// EpsBearer bearer(q);
	// lteHelper->ActivateDataRadioBearer(ueDevs,bearer);
	// // configure all the simulation scenario here...

	GbrQosInformation qos;
	qos.gbrDl = 180000; // Downlink GBR
	qos.gbrUl = qos.gbrDl; // Uplink GBR
	qos.mbrDl = qos.gbrDl*3; // Downlink MBR
	qos.mbrUl = qos.gbrDl*3; // Uplink MBR

	// GbrQosInformation qos1;
	// qos.gbrDl = 500000; // Downlink GBR
	// qos.gbrUl = qos.gbrDl; // Uplink GBR
	// qos.mbrDl = qos.gbrDl*3; // Downlink MBR
	// qos.mbrUl = qos.gbrDl*3; // Uplink MBR 

	EpsBearer bearer(q, qos);
	for (uint16_t i=0; i < ueNodes.GetN(); i++){
		if(i < numberOfUEs/2)
		{
			lteHelper->ActivateDedicatedEpsBearer (ueDevs.Get(i), bearer, EpcTft::Default ());
		}
		else
		{	EpsBearer bearer1(q1);
			lteHelper->ActivateDedicatedEpsBearer (ueDevs.Get(i), bearer1, EpcTft::Default ());
		}
	}
	// Ptr<FlowMonitor> flowMonitor;
	// FlowMonitorHelper flowHelper;
	// flowMonitor = flowHelper.InstallAll();
	// flowMonitor->SetAttribute ("DelayBinWidth", DoubleValue(0.001));
	// flowMonitor->SetAttribute ("JitterBinWidth",DoubleValue (0.001));
	// for (uint16_t j=0; j<10; j++){
  //    Ptr<Node> ueNode = ueNodes.Get (j);
  //    flowMonitor = flowHelper.Install (ueNode);
  //}
	// AnimationInterface anim ("finalv2.xml");
	// anim.SetMaxPktsPerTraceFile(100000);
	// Config::SetDefault ("ns3::ConfigStore::Filename", StringValue ("output-attributes.txt"));
	// Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("RawText"));
	// Config::SetDefault ("ns3::ConfigStore::Mode", StringValue ("Save"));

	// ConfigStore outputConfig;
	// outputConfig.ConfigureDefaults ();
	// outputConfig.ConfigureAttributes ();
	lteHelper->EnableDlMacTraces ();
  	lteHelper->EnableRlcTraces ();

	Simulator::Stop(Seconds(simTime));
	Simulator::Run();
	// flowMonitor->SerializeToXmlFile("finalv2.flowmon", true, true);
	Simulator::Destroy();

	return 0;
}